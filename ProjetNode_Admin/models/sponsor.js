/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
var db = require('../configDb');



module.exports.GestionDesSponsors = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT SPONOM as nom, SPOSECTACTIVITE as act, s.SPONUM as num FROM sponsor s order by sponom asc";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getSponsor = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT SPONOM as nom, SPOSECTACTIVITE as act, s.SPONUM as num FROM sponsor s WHERE s.SPONUM="+data;
            console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getSponsorEcu = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT f.ECUNUM as ecunum, ECUNOM FROM finance f INNER JOIN ecurie e ON e.ECUNUM=f.ECUNUM WHERE SPONUM="+data;
            console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.ListerEcuries= function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT ECUNUM as num, ECUNOM as ecu FROM ecurie ORDER BY ECUNOM ASC";
            console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};
module.exports.AjoutBdd = function (data, callback) {
    // connection à la base
    console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            connexion.query('INSERT INTO sponsor SET ?', data[0], function(err, result) {
                console.log(result.insertId);
                console.log(data);
                if (data[1] !="aucune")
                {
                    var sql ="INSERT INTO finance (ECUNUM, SPONUM) VALUES ("+connexion.escape(data[1])+", "+connexion.escape(result.insertId)+")";
                    connexion.query(sql, data, callback);
                }
                else
                {
                    connexion.query("SELECT SPONOM FROM sponsor", callback);
                }

            });

            connexion.release();
        }

    });
};

module.exports.SupprBdd = function (data, callback) {
    // connection à la base
    console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql =" DELETE FROM finance where SPONUM='"+data+"'";
						connexion.query(sql);
            var sql =" DELETE FROM sponsor where SPONUM='"+data+"'";
						connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.SupprFinanceBdd = function (data, callback) {
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql =" DELETE FROM finance where ECUNUM="+connexion.escape(data);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.ModifBdd = function (data, callback) {

    db.getConnection(function (err, connexion) {
        if (!err) {
            // s'il n'y a pas d'erreur de connexion
            if(data.ECUNUM!='aucune')
            {
                var sql = "INSERT INTO finance (ECUNUM, SPONUM) VALUES ("+connexion.escape(data.ECUNUM)+", "+data.SPONUM+")";
                connexion.query(sql);
            }
            var sql = "UPDATE sponsor SET SPONOM=" + connexion.escape(data.SPONOM) + ", SPOSECTACTIVITE=" + connexion.escape(data.SPOSECTACTIVITE) + " WHERE SPONUM=" + connexion.escape(data.SPONUM);
            connexion.query(sql, callback);
            connexion.release();
        }
    })
};
