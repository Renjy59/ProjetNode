/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
var db = require('../configDb');

/*
* Récupérer l'intégralité les écuries avec l'adresse de la photo du pays de l'écurie
* @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
*/
module.exports.getListeEcurie = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT ecunum, payadrdrap, ecunom FROM ecurie e INNER JOIN pays p ";
						sql= sql + "ON p.paynum=e.paynum ORDER BY ecunom";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.detailsEcurieSelec = function (data, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT ECUNUM, FPNOM as fnom, ECUPOINTS as points, ECUNOM as nom, ECUNOMDIR as dir, ECUADRSIEGE as addr, pays.PAYNOM as paynum, ECUADRESSEIMAGE as img FROM `ecurie` INNER JOIN pays on pays.paynum = ecurie.paynum INNER JOIN fourn_pneu ON fourn_pneu.FPNUM = ecurie.FPNUM WHERE ECUNUM ="+data+"";

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.detailsSponsor = function (data, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT sponom as snom FROM sponsor sp  INNER JOIN finance f on f.sponum = sp.sponum INNER JOIN ecurie e ON  e.ECUNUM= .f.ECUNUM WHERE e.ECUNUM="+data+"";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.getPilotebyEcurie = function (data, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PHOADRESSE as adresse, PILNOM as nom, PILPRENOM as prenom FROM `pilote` INNER JOIN photo ON photo.PILNUM = pilote.PILNUM WHERE ECUNUM ="+data+"";

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.getVoituresbyEcurie = function (data, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT TYPELIBELLE as nom , VOIADRESSEIMAGE as img, VOINUM FROM `voiture` INNER JOIN type_voiture ON type_voiture.TYPNUM = voiture.TYPNUM where ecunum ="+data+"";
            console.log(data);
            console.log (sql);

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.gestionDesEcuries = function (callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT ECUNUM as num, ECUNOM as nom, ECUNOMDIR as directeur, ECUPOINTS as points FROM `ecurie` ORDER BY ECUNOM ASC";
            console.log (sql);

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.ListerPays= function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT PAYNUM as num, PAYNOM as nat FROM pays ORDER BY PAYNAT ASC";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.ListerFournisseurPneus = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT FPNUM as num, FPNOM as nom FROM fourn_pneu ORDER BY FPNOM ASC";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.AjoutBdd = function (data, callback) {
    // connection à la base
        console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            connexion.query('INSERT INTO ecurie SET ?', data, callback);
            connexion.release();
        }
    });
};

module.exports.SupprBdd = function (data, callback) {
    // connection à la base
    console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql="UPDATE pilote SET ECUNUM=NULL WHERE ECUNUM="+connexion.escape(data);
            connexion.query(sql);

            var sql =" DELETE FROM voiture where ECUNUM='"+data+"'";
            connexion.query(sql);

            var sql = "DELETE FROM finance WHERE ECUNUM ='"+data+"'";
            connexion.query(sql);

            var sql = "DELETE FROM ecurie WHERE ECUNUM ='"+data+"'";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.ModifBdd = function (data, callback) {

    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion

            if(data.image=='')
            {
                var sql = "UPDATE ecurie SET ECUNOM="+connexion.escape(data.ECUNOM)+", ECUNOMDIR="+connexion.escape(data.ECUNOMDIR)+", ECUPOINTS="+connexion.escape(data.ECUPOINTS)+", ECUADRSIEGE="+connexion.escape(data.ECUADRSIEGE)+", PAYNUM="+connexion.escape(data.PAYNUM)+"WHERE ECUNUM="+connexion.escape(data.ECUNUM);
            }
            else
            {
                var sql = "UPDATE ecurie SET ECUADRESSEIMAGE="+connexion.escape(data.image)+", ECUNOM="+connexion.escape(data.ECUNOM)+", ECUNOMDIR="+connexion.escape(data.ECUNOMDIR)+", ECUPOINTS="+connexion.escape(data.ECUPOINTS)+", ECUADRSIEGE="+connexion.escape(data.ECUADRSIEGE)+", PAYNUM="+connexion.escape(data.PAYNUM)+"WHERE ECUNUM="+connexion.escape(data.ECUNUM);
            }
            console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};

