var db = require('../configDb');

module.exports.listeGrandPrix = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT GPNOM, GPNUM FROM `grandprix`";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getResultatCourse = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT c.PILNUM, PILNOM, PILPRENOM, TEMPSCOURSE FROM pilote p INNER JOIN course c ON c.PILNUM=p.PILNUM WHERE c.GPNUM="+data+" ORDER BY TEMPSCOURSE ASC ";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getClassement = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PTPLACE, PTNBPOINTSPLACE FROM points ORDER BY PTPLACE ASC ";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.SupprBdd = function (data, callback) {
    // connection à la base
    console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL

            var sql =" DELETE FROM course where PILNUM='"+data+"'";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getPilote = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PILNUM, PILNOM FROM pilote ORDER BY PILNOM";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.AjoutPiloteBdd = function (data, callback) {
    // connection à la base
    console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            connexion.query('INSERT INTO course SET ?', data, callback);
            connexion.release();
        }
    });
};
