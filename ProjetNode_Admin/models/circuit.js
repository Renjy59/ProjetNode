var db = require('../configDb');

module.exports.getListeCircuit = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT CIRNOM, c.CIRNUM, PAYADRDRAP FROM circuit c INNER JOIN pays p ON p.PAYNUM=c.PAYNUM";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getDetailCircuit = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT CIRNOM, c.CIRNUM, CIRLONGUEUR, CIRNBSPECTATEURS, CIRADRESSEIMAGE, CIRTEXT, PAYNOM FROM circuit c INNER JOIN pays p ON p.PAYNUM=c.PAYNUM WHERE CIRNUM="+data+"";
            console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.GestionsDesCircuits = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT CIRNUM as num, CIRNOM as nom , CIRLONGUEUR as longueur, CIRNBSPECTATEURS as spec from circuit order by CIRNOM asc";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.ListerPays= function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT PAYNUM as num, PAYNOM as nat FROM pays ORDER BY PAYNAT ASC";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.AjoutBdd = function (data, callback) {
    // connection à la base
        console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            connexion.query('INSERT INTO circuit SET ?', data, callback);
            connexion.release();
        }
    });
};
module.exports.SupprBdd = function (data, callback) {
    // connection à la base
        console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql =" DELETE course FROM course INNER JOIN grandprix on course.GPNUM = grandprix.GPNUM where grandprix.cirnum='"+data+"'";
            connexion.query(sql);

            var sql = "DELETE FROM grandprix WHERE cirnum ='"+data+"'";
            connexion.query(sql);

            var sql = "DELETE FROM circuit WHERE cirnum ='"+data+"'";

            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.ModifBdd = function (data, callback) {

    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion

            if(data.image=='')
            {
                var sql = "UPDATE circuit SET PAYNUM="+connexion.escape(data.nationalite)+", CIRNOM="+connexion.escape(data.nom)+", CIRLONGUEUR="+connexion.escape(data.longueur)+", CIRNBSPECTATEURS="+connexion.escape(data.spec)+", CIRTEXT="+connexion.escape(data.texte)+"WHERE CIRNUM="+connexion.escape(data.num);
            }
            else
            {
                var sql = "UPDATE circuit SET PAYNUM="+connexion.escape(data.nationalite)+", CIRNOM="+connexion.escape(data.nom)+", CIRLONGUEUR="+connexion.escape(data.longueur)+", CIRNBSPECTATEURS="+connexion.escape(data.spec)+", CIRADRESSEIMAGE="+connexion.escape(data.image)+", CIRTEXT="+connexion.escape(data.texte)+"WHERE CIRNUM="+connexion.escape(data.num);
            }
            console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};
