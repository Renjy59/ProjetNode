/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */
var db = require('../configDb');


module.exports.getPilotes = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PILNUM, PILNOM as nom , PILPRENOM as prenom, month(PILDATENAIS) as mois, day(PILDATENAIS) as jour,year(PILDATENAIS) as annee  FROM pilote order by nom asc";
            console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getLettreRepertoire = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT DISTINCT(LEFT(`PILNOM`, 1)) as lettre FROM `pilote` ORDER BY `PILNOM`";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getPiloteByLettre = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT pi.PILNUM, PILNOM, PILPRENOM, PHOADRESSE FROM pilote pi "
            sql = sql + "INNER JOIN photo ph ON pi.PILNUM=ph.PILNUM WHERE PHONUM=1 AND LEFT(`PILNOM`, 1)='"+data+"'";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getDetailPilote = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT pi.PILNUM, ECUNOM, PILNOM, PILPRENOM, PILPOIDS, PILPOINTS, date_format(PILDATENAIS, '%m') as mois, date_format(PILDATENAIS, '%d') as jour,year(PILDATENAIS) as annee, PAYNAT, PILTAILLE, PILTEXTE, PHOADRESSE FROM pilote pi INNER JOIN pays pa ON pi.PAYNUM=pa.PAYNUM INNER JOIN photo ph ON pi.PILNUM=ph.PILNUM LEFT JOIN ecurie e ON pi.ECUNUM=e.ECUNUM WHERE pi.PILNUM="+data+" AND PHONUM=1";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getDetailPiloteSpon = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT SPONOM, SPOSECTACTIVITE FROM sponsorise sp INNER JOIN sponsor s ON sp.SPONUM=s.SPONUM INNER JOIN pilote pi ON sp.PILNUM=pi.PILNUM WHERE pi.PILNUM="+data+"";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getDetailPilotePhoto = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PHOCOMMENTAIRE, PHOADRESSE FROM pilote pi INNER JOIN photo ph ON pi.PILNUM=ph.PILNUM WHERE pi.PILNUM="+data+" AND PHONUM>1";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};


module.exports.ListerNationalites= function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PAYNUM as num, PAYNAT as nat FROM pays ORDER BY PAYNAT ASC";
            console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};
module.exports.ListerEcuries= function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT ECUNUM as num, ECUNOM as ecu FROM ecurie ORDER BY ECUNOM ASC";
            console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.AjoutBdd = function (data, callback) {
    // connection à la base
    console.log(data);
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            connexion.query('INSERT INTO pilote SET ?', data, callback);
            connexion.release();
        }
    });
};

module.exports.ModifBdd = function (data, callback) {

    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion

            var sql = "UPDATE pilote SET PILNOM="+connexion.escape(data.PILNOM)+", PILPRENOM="+connexion.escape(data.PILPRENOM)+", PILDATENAIS="+connexion.escape(data.PILDATENAIS)+", PAYNUM="+connexion.escape(data.PAYNUM)+", ECUNUM="+connexion.escape(data.ECUNUM)+", PILPOINTS="+connexion.escape(data.PILPOINTS)+", PILPOIDS="+connexion.escape(data.PILPOIDS)+", PILTAILLE="+connexion.escape(data.PILTAILLE)+", PILTEXTE="+connexion.escape(data.PILTEXTE)+" WHERE PILNUM="+connexion.escape(data.PILNUM);
            console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};

module.exports.SupprBdd = function (data, callback) {

    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion

            var sql = "DELETE FROM essais WHERE PILNUM="+data
            //console.log(sql);
            connexion.query(sql);

            var sql = "DELETE FROM course WHERE PILNUM="+data
            //console.log(sql);
            connexion.query(sql);

            var sql = "DELETE FROM photo WHERE PILNUM="+data
            //console.log(sql);
            connexion.query(sql);

            var sql = "DELETE FROM sponsorise WHERE PILNUM="+data
            //console.log(sql);
            connexion.query(sql);

            var sql = "DELETE FROM pilote WHERE PILNUM="+data;
            //console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};
//
//
// DELETE FROM essais
// WHERE PILNUM IN
//   ( SELECT pilnum from pilote where ECUNUM=1);
// DELETE FROM course
// WHERE PILNUM IN
//   ( SELECT pilnum from pilote where ECUNUM=1);
//   DELETE FROM photo
// WHERE PILNUM IN
//   ( SELECT pilnum from pilote where ECUNUM=1);
//   DELETE FROM sponsorise
// WHERE PILNUM IN
//   ( SELECT pilnum from pilote where ECUNUM=1);
//
// DELETE FROM pilote where ECUNUM=1;
// DELETE FROM finance where ECUNUM=1;
// DELETE FROM ecurie where ECUNUM=1;
// DELETE FROM voiture where ECUNUM=1;
// DELETE FROM finance where ECUNUM=1;
// DELETE FROM ecurie where ECUNUM=1
