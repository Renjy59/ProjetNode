var model = require('../models/pilote.js');
var async = require('async');


// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S

module.exports.Repertoire = 	function(request, response){
    response.title = 'Répertoire des pilotes';
    model.getPilotes( function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        console.log(result)
        response.pilotes = result;

        response.render('repertoirePilotes', response);
    });
};
module.exports.Ajout = 	function(request, response){
    response.title = 'Répertoire des pilotes';
    async.parallel([
            function(callback)
            {
                model.ListerNationalites( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.ListerEcuries( function (err, result)
                {
                    callback(null, result);
                });
            }
        ],
        function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.nat = result[0];
            response.ecu = result[1];

            response.render('ajoutPilote', response);
        });
};

module.exports.Add = 	function(request, response){
    var data = {
        PILNOM : request.body.nom,
        PILPRENOM :request.body.prenom,
        PILDATENAIS : request.body.date,
        PAYNUM : request.body.nationalite,
        ECUNUM : request.body.ecurie,
        PILPOINTS : request.body.points,
        PILPOIDS : request.body.poids,
        PILTAILLE : request.body.taille,
        PILTEXTE : request.body.description
    };

    //console.log(data);

    response.title = 'Répertoire des pilotes';

    model.AjoutBdd(data, function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.action="L'ajout a bien été effectué"
        response.render('actionPilote', response);
    });
};

module.exports.ModifForm = 	function(request, response){
    response.title = 'Répertoire des pilotes';
    var data = request.params.numPilote;
    async.parallel([
            function(callback)
            {
                model.ListerNationalites( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.ListerEcuries( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.getDetailPilote(data, (function (errE, resE)
                {
                    callback(null, resE);
                }));
            }
        ],
        function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.nat = result[0];
            response.ecu = result[1];
            response.infoPil = result[2];
            console.log(result[2]);
            response.render('modifPilotes', response);
        });
};

module.exports.Modif = 	function(request, response){
    var data = {
        PILNUM : request.body.num,
        PILNOM : request.body.nom,
        PILPRENOM :request.body.prenom,
        PILDATENAIS : request.body.date,
        PAYNUM : request.body.nationalite,
        ECUNUM : request.body.ecurie,
        PILPOINTS : request.body.points,
        PILPOIDS : request.body.poids,
        PILTAILLE : request.body.taille,
        PILTEXTE : request.body.description
    };

    console.log(data);

    response.title = 'Répertoire des pilotes';
    model.ModifBdd(data, function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.action="La modification a bien été effectuée"
        response.render('actionPilote', response);
    });
};

module.exports.Supp = 	function(request, response){
    var data = request.body.suppId;

    console.log(data);

    response.title = 'Répertoire des pilotes';
    model.SupprBdd(data, function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.action="La suppression a bien été effectuée";
        response.render('actionPilote', response);
    });
};