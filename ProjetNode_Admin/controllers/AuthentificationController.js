var model = require('../models/connect.js');
var async = require('async');
var sha1 = require('sha1');

module.exports.connexion = function(request, response){
    response.title = 'Connexion';
    //request.session.destroy()
    var data = [request.body.username, sha1(request.body.password)];

    model.getConnexion( data, function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        response.connect = result;
        if(response.connect[0].resultat == 1)
        {
            request.session.login = request.body.username;
            request.session.save();
        }

        response.render('connexion', response);
    } );
};

module.exports.deconnexion = function(request, response)
{
    response.title = 'Deconnexion';
    response.deconnect = request.session.login;
    request.session.destroy();
    response.render('deconnexion', response)

};
