var model = require('../models/circuit.js');
var async = require('async'),
    formidable = require('formidable'),
    http = require('http'),
    util = require('util'),
    fs   = require('fs-extra');

// ////////////////////// L I S T E R     C I R C U I T S

module.exports.ListerCircuit = function(request, response){
  response.title = 'Liste des circuits';
  async.parallel([
    function(callback)
    {
      model.GestionsDesCircuits( function (err, result)
      {
        callback(null, result);
      });
    }
  ],
  function (err, result)
  {
    if (err) {
      // gestion de l'erreur
      console.log(err);
      return;
    }

    response.circuit = result[0];
    response.render('listerCircuit', response);
  });
};

module.exports.Ajout =  function(request, response){
  response.title = 'Répertoire des circuits';
  async.parallel([
    function(callback)
    {
      model.ListerPays( function (err, result)
      {
        callback(null, result);
      });
    }
  ],
  function (err, result)
  {
    if (err) {
      // gestion de l'erreur
      console.log(err);
      return;
    }
    response.nat = result[0];
    response.render('ajoutCircuit', response);
  });
};

module.exports.Add =  function(request, response){
  response.title = 'Répertoire des circuits';

  var form = new formidable.IncomingForm();
  form.parse(request, function (err, fields, files) {
    var data=fields;
    data.CIRADRESSEIMAGE=files.image.name;
    model.AjoutBdd(data, function (err, result)
    {
      if (err) {
        // gestion de l'erreur
        console.log(err);
        return;
      }
      response.action="L'ajout a bien été effectué"
      response.render('actionCircuit', response);
    });
  });
  form.on('fileBegin', function(name, file) {
    console.log(file.path);
  });
  form.on('progress', function(bytesReceived, bytesExpected) {
    var percent_complete = (bytesReceived / bytesExpected) * 100;
    console.log(percent_complete.toFixed(2));
  });
  form.on('end', function (fields, files) {
    if(this.openedFiles==undefined){
      return;
    }

    var temp_path = this.openedFiles[0].path;
    /* The file name of the uploaded file */
    var file_name = this.openedFiles[0].name;
    /* Location where we want to copy the uploaded file */
    var new_location = './../ProjetNode_Public/public/image/circuit/';
    fs.copy(temp_path, new_location + file_name, function (err) {
      if (err) {
        console.error(err);
      } else {
        console.log("success!");
        // Delete the "temp" file
        fs.unlink(temp_path, function(err) {
          if (err) {
            console.error(err);
            console.log("TROUBLE deletion temp !");
          } else {
            console.log("success deletion temp !");
          }
        });
      }
    });
  });
  return;
};

module.exports.Delete =  function(request, response){
  var data = request.body.suppId;
  console.log(data);
  response.title = 'Répertoire des circuits';
  model.SupprBdd(data, function (err, result)
  {
    if (err) {
      // gestion de l'erreur
      console.log(err);
      return;
    }
    response.action="La suppression a bien été effectuée";
    response.render('actionCircuit', response);
  });
};

module.exports.Modify =  function(request, response){
  var data = request.params.numCircuit;
  console.log(data);
  response.title = 'Répertoire des circuits';
  async.parallel([
        function(callback)
        {
          model.ListerPays( function (err, result)
          {
            callback(null, result);
          });
        },
        function(callback)
        {
          model.getDetailCircuit(data, (function (errE, resE)
          {
            callback(null, resE);
          }));
        }
      ],
  function (err, result)
  {
    if (err) {
      // gestion de l'erreur
      console.log(err);
      return;
    }
    response.infoCir = result[1];
    response.nat = result[0];
    response.render('modifCircuit', response);
  });
};

module.exports.Modif =  function(request, response){

  response.title = 'Répertoire des circuits';

  var form = new formidable.IncomingForm();
  form.parse(request, function (err, fields, files) {
    var data=fields;
    data.image=files.image.name;
    model.ModifBdd(data, function (err, result)
    {
      if (err) {
        // gestion de l'erreur
        console.log(err);
        return;
      }
      response.action="La modification a bien été effectuée"
      response.render('actionCircuit', response);
    });
  });
  form.on('fileBegin', function(name, file) {
    console.log(file.path);
  });
  form.on('progress', function(bytesReceived, bytesExpected) {
    var percent_complete = (bytesReceived / bytesExpected) * 100;
    console.log(percent_complete.toFixed(2));
  });
  form.on('end', function (fields, files) {
    if(this.openedFiles==undefined){
      return;
    }

    var temp_path = this.openedFiles[0].path;
    /* The file name of the uploaded file */
    var file_name = this.openedFiles[0].name;
    /* Location where we want to copy the uploaded file */
    var new_location = './../ProjetNode_Public/public/image/circuit/';
    fs.copy(temp_path, new_location + file_name, function (err) {
      if (err) {
        console.error(err);
      } else {
        console.log("success!");
        // Delete the "temp" file
        fs.unlink(temp_path, function(err) {
          if (err) {
            console.error(err);
            console.log("TROUBLE deletion temp !");
          } else {
            console.log("success deletion temp !");
          }
        });
      }
    });
  });
  return;
};
