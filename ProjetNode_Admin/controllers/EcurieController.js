
var model = require('../models/ecurie.js');
var async = require('async'),
    formidable = require('formidable'),
    http = require('http'),
    util = require('util'),
    fs   = require('fs-extra');

module.exports.ListerEcurie = function(request, response){
   response.title = 'Liste des écuries';

   async.parallel([
     function(callback)
     {
       model.gestionDesEcuries( function (err, result)
       {
          callback(null, result);
        });
      }
    ],
      function (err, result)
      {
      if (err) {
         // gestion de l'erreur
         console.log(err);
         return;
      }
    response.ecurie = result[0];
   response.render('listerEcurie', response);
        });
};
module.exports.Ajout = function(request, response){
   response.title = 'Liste des écuries';

   async.parallel([
     function(callback)
     {
       model.ListerPays( function (err, result)
       {
          callback(null, result);
        });
      },
      function(callback)
      {
        model.ListerFournisseurPneus( function (err, result)
        {
           callback(null, result);
         });
       }

    ],
      function (err, result)
      {
      if (err) {
         // gestion de l'erreur
         console.log(err);
         return;
      }
    response.nat = result[0];
    response.fp = result[1];

   response.render('ajoutEcurie', response);
        });
};

module.exports.Add =  function(request, response){
    var form = new formidable.IncomingForm();
    form.parse(request, function (err, fields, files) {
        var data=fields;
        data.ECUADRESSEIMAGE=files.image.name;
        model.AjoutBdd(data, function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.action="L'ajout a bien été effectué"
            response.render('actionEcurie', response);
        });
    });
    form.on('fileBegin', function(name, file) {
        console.log(file.path);
    });
    form.on('progress', function(bytesReceived, bytesExpected) {
        var percent_complete = (bytesReceived / bytesExpected) * 100;
        console.log(percent_complete.toFixed(2));
    });
    form.on('end', function (fields, files) {
        if(this.openedFiles==undefined){
            return;
        }

        var temp_path = this.openedFiles[0].path;
        /* The file name of the uploaded file */
        var file_name = this.openedFiles[0].name;
        /* Location where we want to copy the uploaded file */
        var new_location = './../ProjetNode_Public/public/image/ecurie/';
        fs.copy(temp_path, new_location + file_name, function (err) {
            if (err) {
                console.error(err);
            } else {
                console.log("success!");
                // Delete the "temp" file
                fs.unlink(temp_path, function(err) {
                    if (err) {
                        console.error(err);
                        console.log("TROUBLE deletion temp !");
                    } else {
                        console.log("success deletion temp !");
                    }
                });
            }
        });
    });
    return;
};

module.exports.Delete =  function(request, response){
    var data = request.body.suppId;

    response.title = 'Répertoire des ecuries';
    model.SupprBdd(data, function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.action="La suppression a bien été effectuée";
        response.render('actionEcurie', response);
    });
};

module.exports.Modify =  function(request, response){
    var data = request.params.numEcurie;
    console.log(data);
    response.title = 'Répertoire des circuits';
    async.parallel([
            function(callback)
            {
                model.ListerPays( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.detailsEcurieSelec(data, (function (errE, resE)
                {
                    callback(null, resE);
                }));
            }
        ],
        function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.infoEcu = result[1];
            response.nat = result[0];
            response.render('modifEcurie', response);
        });
};

module.exports.Modif =  function(request, response){

    response.title = 'Répertoire des circuits';

    var form = new formidable.IncomingForm();
    form.parse(request, function (err, fields, files) {
        var data=fields;
        data.image=files.image.name;
        model.ModifBdd(data, function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.action="La modification a bien été effectuée"
            response.render('actionEcurie', response);
        });
    });
    form.on('fileBegin', function(name, file) {
        console.log(file.path);
    });
    form.on('progress', function(bytesReceived, bytesExpected) {
        var percent_complete = (bytesReceived / bytesExpected) * 100;
        console.log(percent_complete.toFixed(2));
    });
    form.on('end', function (fields, files) {
        if(this.openedFiles==undefined){
            return;
        }

        var temp_path = this.openedFiles[0].path;
        /* The file name of the uploaded file */
        var file_name = this.openedFiles[0].name;
        /* Location where we want to copy the uploaded file */
        var new_location = './../ProjetNode_Public/public/image/ecurie/';
        fs.copy(temp_path, new_location + file_name, function (err) {
            if (err) {
                console.error(err);
            } else {
                console.log("success!");
                // Delete the "temp" file
                fs.unlink(temp_path, function(err) {
                    if (err) {
                        console.error(err);
                        console.log("TROUBLE deletion temp !");
                    } else {
                        console.log("success deletion temp !");
                    }
                });
            }
        });
    });
    return;
};


