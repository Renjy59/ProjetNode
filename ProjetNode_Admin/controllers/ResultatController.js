var model = require('../models/resultat.js');
var async = require('async');


// //////////////////////////L I S T E R    R E S U L T A T S
module.exports.ListerResultat = function(request, response){
	response.title = 'Liste des résulats des grands prix';
	async.parallel([
			function(callback)
			{
				model.listeGrandPrix( function (err, result)
				{
					callback(null, result);
				});
			}
		],
		function (err, result)
		{
			if (err) {
				// gestion de l'erreur
				console.log(err);
				return;
			}

			response.gp = result[0];
			response.render('listerResultat', response);
		});
};
module.exports.DetailsResultat = function(request, response){
	var data = request.body.gp;
	console.log(data);
	async.parallel([
			function(callback)
			{
				model.listeGrandPrix( function (err, result)
				{
					callback(null, result);
				});
			},
			function(callback)
			{
				model.getResultatCourse(data, (function (errE, resE)
				{
					callback(null, resE);
				}));
			},
			function(callback)
			{
				model.getClassement((function (errF, resF)
				{
					callback(null, resF);
				}));
			},
			function(callback)
			{
				model.getPilote((function (errF, resF)
				{
					callback(null, resF);
				}));
			}
		],
		function (err, result)
		{
			if (err) {
				// gestion de l'erreur
				console.log(err);
				return;
			}


			response.resultatCourse = result[1];
			response.classement = result[2];
			response.gp = result[0];
			response.pilote = result[3];

			response.render('listerResultat', response);
		});
};

module.exports.DetailsResultatAndSupp = function(request, response){
	var data = request.body.gp2;
	var dataS = request.body.suppId;
	async.parallel([
			function(callback)
			{
				model.SupprBdd(dataS, function (err, result)
				{
					callback(null, result);
				});
			},
			function(callback)
			{
				model.listeGrandPrix( function (err, result)
				{
					callback(null, result);
				});
			},
			function(callback)
			{
				model.getResultatCourse(data, (function (errE, resE)
				{
					callback(null, resE);
				}));
			},
			function(callback)
			{
				model.getClassement((function (errF, resF)
				{
					callback(null, resF);
				}));
			},
			function(callback)
			{
				model.getPilote((function (errF, resF)
				{
					callback(null, resF);
				}));
			}
		],
		function (err, result)
		{
			if (err) {
				// gestion de l'erreur
				console.log(err);
				return;
			}


			response.resultatCourse = result[2];
			response.classement = result[3];
			response.gp = result[1];
			response.pilote = result[4];

			response.render('listerResultat', response);
		});
};

module.exports.DetailsResultatAndAdd = function(request, response){
	var data = request.body.gp2;
	var dataAdd  = {
		PILNUM : request.body.piloteInfo,
		GPNUM : request.body.gp2,
		TEMPSCOURSE :request.body.min+":"+request.body.sec+":"+request.body.cen
	};
	async.parallel([
			function(callback)
			{
				model.AjoutPiloteBdd(dataAdd, function (err, result)
				{
					callback(null, result);
				});
			},
			function(callback)
			{
				model.listeGrandPrix( function (err, result)
				{
					callback(null, result);
				});
			},
			function(callback)
			{
				model.getResultatCourse(data, (function (errE, resE)
				{
					callback(null, resE);
				}));
			},
			function(callback)
			{
				model.getClassement((function (errF, resF)
				{
					callback(null, resF);
				}));
			},
			function(callback)
			{
				model.getPilote((function (errF, resF)
				{
					callback(null, resF);
				}));
			}
		],
		function (err, result)
		{
			if (err) {
				// gestion de l'erreur
				console.log(err);
				return;
			}


			response.resultatCourse = result[2];
			response.classement = result[3];
			response.gp = result[1];
			response.pilote = result[4];

			response.render('listerResultat', response);
		});
};
