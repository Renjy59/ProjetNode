var model = require('../models/sponsor.js');
var async = require('async');

  // ////////////////////////////////////////////// A C C U E I L
module.exports.Index = function(request, response){
    response.title = "Bienvenue sur le site de WROOM (IUT du Limousin).";
    async.parallel([
      function(callback)
      {
        model.GestionDesSponsors( function (err, result)
        {
           callback(null, result);
         });
       }
     ],
       function (err, result)
       {
       if (err) {
          // gestion de l'erreur
          console.log(err);
          return;
       }

       response.sponsor = result[0];

       response.render('listerSponsors', response);
     });
};
module.exports.Ajout = 	function(request, response){
    response.title = 'Répertoire des pilotes';

    model.ListerEcuries( function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.ecurie = result[0];

        response.render('ajoutSponsor', response);
    });
};

module.exports.Add = 	function(request, response){
    var data = {
        SPONOM : request.body.nom,
        SPOSECTACTIVITE :request.body.sa
    };
    var dataEcurieSponso = request.body.es;
    var tableData = [data, dataEcurieSponso]
    model.AjoutBdd(tableData, function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.action="L'ajout a bien été effectué";
        response.render('actionSponsor', response);
    });

};
module.exports.Supp =  function(request, response){
    var data = request.body.suppId;

    response.title = 'Répertoire des sponsors';
    model.SupprBdd(data, function (err, result)
    {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.action="La suppression a bien été effectuée";
        response.render('actionSponsor', response);
    });
};

module.exports.Modify = function(request, response){
    response.title = 'Répertoire des sponsors';
    var data = request.params.numSponsor;
    async.parallel([
            function(callback)
            {
                model.ListerEcuries( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.getSponsor(data, (function (errE, resE)
                {
                    callback(null, resE);
                }));
            },
            function(callback)
            {
                model.getSponsorEcu(data, (function (errE, resE)
                {
                    callback(null, resE);
                }));
            }
        ],
        function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.infSpon = result[1];
            response.ecurie = result[0];
            response.infSponEcu = result[2];
            response.render('modifSponsor', response);
        });
};

module.exports.Modif = 	function(request, response){

    if(request.body.suppId=='')
    {
        var data = {
            SPONUM : request.body.num,
            SPONOM : request.body.nom,
            SPOSECTACTIVITE : request.body.sa,
            ECUNUM : request.body.es
        };

        console.log(data);

        response.title = 'Répertoire des pilotes';
        model.ModifBdd(data, function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.action="La modification a bien été effectuée"
            response.render('actionSponsor', response);
        });
    }
    else
    {
        var data = request.body.suppId;
        console.log("oui"+data);
        response.title = 'Répertoire des sponsors';
        model.SupprFinanceBdd(data, function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            response.action="La modification a bien été effectuée"
            response.render('actionSponsor', response);
        });
    }
};
