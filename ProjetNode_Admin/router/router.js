var HomeController = require('./../controllers/HomeController');
var ResultatController = require('./../controllers/ResultatController');
var EcurieController = require('./../controllers/EcurieController');
var PiloteController = require('./../controllers/PiloteController');
var CircuitController = require('./../controllers/CircuitController');
var AuthentificationController = require('./../controllers/AuthentificationController');
var SponsorController = require('./../controllers/SponsorController');

// Routes
module.exports = function(app){
// Main Routes
    app.get('/connexion', HomeController.Index);
    app.get('/deconnexion', AuthentificationController.deconnexion);
    app.post('/connexion/login', AuthentificationController.connexion);

// Pilote Routes
    app.get('/pilotes', PiloteController.Repertoire);
    app.get('/ajoutPilote', PiloteController.Ajout);
    app.get('/modifPilotes/:numPilote', PiloteController.ModifForm);
    app.post('/pilotes/add', PiloteController.Add);
    app.post('/pilotes/modif', PiloteController.Modif);
    app.post('/pilotes/supp', PiloteController.Supp);

    // circuits
    app.get('/circuits', CircuitController.ListerCircuit);
    app.post('/circuits/modif', CircuitController.Modif);
    app.post('/circuits/add', CircuitController.Add);
    app.get('/modifCircuit/:numCircuit', CircuitController.Modify);
    app.get('/ajoutCircuit', CircuitController.Ajout);
    app.post('/circuits/supp', CircuitController.Delete);


// Ecuries
    app.get('/ecuries', EcurieController.ListerEcurie);
    app.get('/ajoutEcurie', EcurieController.Ajout);
    app.post('/ecuries/modif', EcurieController.Modif);
    app.post('/ecuries/add', EcurieController.Add);
    app.get('/modifEcurie/:numEcurie', EcurieController.Modify);
    app.post('/ecuries/supp', EcurieController.Delete);


    //Résultats
    app.get('/resultats', ResultatController.ListerResultat);
    app.post('/resultats/num', ResultatController.DetailsResultat);
    app.post('/resultats/num/supp', ResultatController.DetailsResultatAndSupp);
    app.post('/resultats/num/add', ResultatController.DetailsResultatAndAdd);

    // sponsors
    app.get('/sponsors', SponsorController.Index);
    app.get('/ajoutSponsor', SponsorController.Ajout);
    app.post('/sponsors/add', SponsorController.Add);
    app.post('/sponsors/supp', SponsorController.Supp);
    app.post('/sponsors/modif', SponsorController.Modif);
    app.get('/modifSponsor/:numSponsor', SponsorController.Modify);




// tout le reste
    app.get('*', HomeController.Index);
    app.post('*', HomeController.Index);

};
