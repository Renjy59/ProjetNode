var model = require('../models/pilote.js');
var async = require('async');
// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S
module.exports.Repertoire = 	function(request, response){
   response.title = 'Répertoire des pilotes';
   model.getLettreRepertoire( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
   response.lettrePilote = result;
      response.render('repertoirePilotes', response);
  } );
} ;

module.exports.RepertoireInfo = 	function(request, response){
   var data = request.params.lettre;
   response.title = 'Répertoire des pilotes';
   //console.log(data);
   async.parallel([
     function(callback)
     {
       model.getLettreRepertoire( function (err, result)
       {
          callback(null, result);
        });
      },
      function(callback)
      {
        model.getPiloteByLettre(data, (function (errE, resE)
        {
             callback(null, resE);
          }));
        }
      ],
      function (err, result)
      {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        //console.log("non");
        response.lettrePilote = result[0];
        response.detailPilote = result[1];
        response.render('repertoirePilotes', response);
      } );
} ;

module.exports.RepertoireInfoPilote = 	function(request, response){
   var data = request.params.pilnum;
   response.title = 'Répertoire des pilotes';
   //console.log(data);
   async.parallel([
     function(callback)
     {
       model.getLettreRepertoire( function (err, result)
       {
          callback(null, result);
        });
      },
      function(callback)
      {
        model.getDetailPilote(data, (function (errE, resE)
        {
             callback(null, resE);
          }));
        },
       function(callback)
       {
           model.getDetailPiloteSpon(data, (function (errF, resF)
           {
               callback(null, resF);
           }));
       },
       function(callback)
       {
           model.getDetailPilotePhoto(data, (function (errG, resG)
           {
               callback(null, resG);
           }));
       }
      ],
      function (err, result)
      {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        //console.log("non");

        response.lettrePilote = result[0];
        response.detailPilotePlus = result[1];
          response.detailSpon = result[2];
          response.detailPhoto = result[3];
        response.render('repertoirePilotes', response);
      } );
} ;
