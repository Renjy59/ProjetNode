var model = require('../models/ecurie.js');
var async = require('async');

// //////////////////////// L I S T E R  C I R C U I T S

/*
 <!--
 * listeEcurie contient par exemple :
 * [
 * { ecunum: 5, payadrdrap: 'AAA',ecunom:'rrr' },
 * { ecunum: 6, payadrdrap: 'BAA' ,ecunom:'ggg'},
 * { ecunum: 7, payadrdrap: 'ACA' ,ecunom:'kkkk'}
 *  ]
 *
 * response.title est passé à main.handlebars via la vue ListerEcurie
 * il sera inclus dans cette balise : <title> {{title}}</title>
 */

module.exports.ListerEcurie = function(request, response){
    response.title = 'Liste des écuries';

    model.getListeEcurie( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeEcurie = result;
        response.render('listerEcurie', response);
    });
};

module.exports.DetaillerEcurie = function(request, response){
    response.title = 'Details de l ecurie ';
    var data = request.params.ecunum;

    async.parallel([
            function(callback)
            {
                model.getListeEcurie( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.getPilotebyEcurie(data, (function (errG, resG)
                {
                    callback(null, resG);
                }));
            },
            function(callback)
            {
                model.detailsEcurieSelec(data, (function (errG, resG)
                {
                    callback(null, resG);
                }));
            },
            function(callback)
            {
                model.getVoituresbyEcurie(data, (function (errG, resG)
                {
                    callback(null, resG);
                }));
            },
            function(callback)
            {
                model.detailsSponsor(data, (function (errG, resG)
                {
                    callback(null, resG);
                }));
            }
        ],
        function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }



            response.listeEcurie = result[0];
            response.detailEcurie = result[2];
            response.voitures = result[3];
            response.piloteEcurie = result[1];
            response.sponsors = result[4];

            response.render('listerEcurie', response);


        });
};
