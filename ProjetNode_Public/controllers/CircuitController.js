var model = require('../models/circuit.js');
var async = require('async');
// ////////////////////// L I S T E R     C I R C U I T S

module.exports.ListerCircuit = function(request, response){
   response.title = 'Lister les circuits';
   model.getListeCircuit( function (err, result) {
      if (err) {
         // gestion de l'erreur
         console.log(err);
         return;
      }

      response.listeCircuit = result;
      response.render('listerCircuit', response);
   } );
};

module.exports.DetailCircuit = 	function(request, response){
    var data = request.params.cirnum;
    response.title = 'Detail du circuit';
    //console.log(data);
    async.parallel([
            function(callback)
            {
                model.getListeCircuit( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.getDetailCircuit(data, (function (errE, resE)
                {
                    callback(null, resE);
                }));
            }
        ],
        function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            //console.log("non");
            response.listeCircuit = result[0];
            response.detailCircuit = result[1];
            console.log(result[1]);
            response.render('listerCircuit', response);
        } );
} ;
