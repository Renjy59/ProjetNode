var model = require('../models/course.js');
var async = require('async');

  // //////////////////////////L I S T E R    R E S U L T A T S
module.exports.ListerResultat = function(request, response){

    response.title = 'Lister les courses';
    model.getListeCourse( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        response.listeCourse = result;
        response.render('listerResultat', response);
    } );
};

module.exports.ResultatCourse = 	function(request, response){
    var data = request.params.gpnum;
    response.title = 'Afficher les resultats';
    //console.log(data);
    async.parallel([
            function(callback)
            {
                model.getListeCourse( function (err, result)
                {
                    callback(null, result);
                });
            },
            function(callback)
            {
                model.getResultatCourse(data, (function (errE, resE)
                {
                    callback(null, resE);
                }));
            },
            function(callback)
            {
                model.getClassement((function (errF, resF)
                {
                    callback(null, resF);
                }));
            },
            function(callback)
            {
                model.getTexteCircuit(data, (function (errG, resG)
                {
                    callback(null, resG);
                }));
            }
        ],
        function (err, result)
        {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }
            //console.log("non");

            response.listeCourse = result[0];
            response.resultatCourse = result[1];
            response.classement = result[2];
            response.circuitTexte = result[3];
            response.render('listerResultat', response);
        } );
} ;
