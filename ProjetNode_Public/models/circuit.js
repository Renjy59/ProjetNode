var db = require('../configDb');

module.exports.getListeCircuit = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT CIRNOM, c.CIRNUM, PAYADRDRAP FROM circuit c INNER JOIN pays p ON p.PAYNUM=c.PAYNUM";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getDetailCircuit = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT CIRNOM, c.CIRNUM, CIRLONGUEUR, CIRNBSPECTATEURS, CIRADRESSEIMAGE, CIRTEXT, PAYNOM FROM circuit c INNER JOIN pays p ON p.PAYNUM=c.PAYNUM WHERE CIRNUM="+data+"";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};