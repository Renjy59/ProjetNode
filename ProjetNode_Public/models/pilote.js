/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
var db = require('../configDb');


module.exports.getLettreRepertoire = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT DISTINCT(LEFT(`PILNOM`, 1)) as lettre FROM `pilote` ORDER BY `PILNOM`";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getPiloteByLettre = function (data, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT pi.PILNUM, PILNOM, PILPRENOM, PHOADRESSE FROM pilote pi "
						sql = sql + "INNER JOIN photo ph ON pi.PILNUM=ph.PILNUM WHERE PHONUM=1 AND LEFT(`PILNOM`, 1)='"+data+"'";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getDetailPilote = function (data, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT pi.PILNUM, ECUNOM, PILNOM, PILPRENOM, PILPOIDS, PILDATENAIS, PAYNAT, PILTAILLE, PILTEXTE, PHOADRESSE FROM pilote pi INNER JOIN pays pa ON pi.PAYNUM=pa.PAYNUM INNER JOIN photo ph ON pi.PILNUM=ph.PILNUM LEFT JOIN ecurie e ON pi.ECUNUM=e.ECUNUM WHERE pi.PILNUM="+data+" AND PHONUM=1";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getDetailPiloteSpon = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT SPONOM, SPOSECTACTIVITE FROM sponsorise sp INNER JOIN sponsor s ON sp.SPONUM=s.SPONUM INNER JOIN pilote pi ON sp.PILNUM=pi.PILNUM WHERE pi.PILNUM="+data+"";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getDetailPilotePhoto = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PHOCOMMENTAIRE, PHOADRESSE FROM pilote pi INNER JOIN photo ph ON pi.PILNUM=ph.PILNUM WHERE pi.PILNUM="+data+" AND PHONUM>1";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};
