/*
 * config.Db contient les parametres de connection à la base de données
 * il va créer aussi un pool de connexions utilisables
 * sa méthode getConnection permet de se connecter à MySQL
 *
 */
var db = require('../configDb');

/*
 * Récupérer l'intégralité les écuries avec l'adresse de la photo du pays de l'écurie
 * @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
 */
module.exports.getListeEcurie = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT ecunum, payadrdrap, ecunom FROM ecurie e INNER JOIN pays p ";
            sql= sql + "ON p.paynum=e.paynum ORDER BY ecunom";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.detailsEcurieSelec = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT FPNOM as fnom, ECUNOM as nom, ECUNOMDIR as dir, ECUADRSIEGE as addr, pays.PAYNOM as paynum, ECUADRESSEIMAGE as img FROM `ecurie` INNER JOIN pays on pays.paynum = ecurie.paynum INNER JOIN fourn_pneu ON fourn_pneu.FPNUM = ecurie.FPNUM WHERE ECUNUM ="+data+"";

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};
module.exports.detailsSponsor = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT sponom as snom FROM sponsor sp  INNER JOIN finance f on f.sponum = sp.sponum INNER JOIN ecurie e ON  e.ECUNUM= .f.ECUNUM WHERE e.ECUNUM="+data+"";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};
module.exports.getPilotebyEcurie = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PHOADRESSE as adresse, PILNOM as nom, PILPRENOM as prenom FROM `pilote` INNER JOIN photo ON photo.PILNUM = pilote.PILNUM WHERE ECUNUM ="+data+"";

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};
module.exports.getVoituresbyEcurie = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT TYPELIBELLE as nom , VOIADRESSEIMAGE as img, VOINUM FROM `voiture` INNER JOIN type_voiture ON type_voiture.TYPNUM = voiture.TYPNUM where ecunum ="+data+"";
            console.log(data);
            console.log (sql);

            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};
