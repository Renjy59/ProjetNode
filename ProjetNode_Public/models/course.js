var db = require('../configDb');

module.exports.getListeCourse = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT GPNOM, GPNUM, PAYADRDRAP FROM grandprix g INNER JOIN circuit c ON c.CIRNUM=g.CIRNUM INNER JOIN pays p ON c.PAYNUM=p.PAYNUM";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getResultatCourse = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PILNOM, PILPRENOM, TEMPSCOURSE FROM pilote p INNER JOIN course c ON c.PILNUM=p.PILNUM WHERE c.GPNUM="+data+" ORDER BY TEMPSCOURSE ASC ";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getClassement = function (callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT PTPLACE, PTNBPOINTSPLACE FROM points ORDER BY PTPLACE ASC ";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};

module.exports.getTexteCircuit = function (data, callback) {
    // connection à la base
    db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT GPCOMMENTAIRE FROM grandprix WHERE GPNUM="+data+"";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
        }
    });
};